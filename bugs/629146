<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<title>Bug 629146 – Guide to writing babl extensions</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<link href="https://bugzilla.gnome.org/" rel="Top"/>
<link href="showdependencytree.cgi?id=629146&amp;hide_resolved=1" rel="Show" title="Dependency Tree"/>
<link href="show_bug.cgi?format=multiple&amp;id=629146" rel="Show" title="Printer-Friendly Version"/>
<link href="skins/standard/global.css" rel="alternate stylesheet" title="Classic"/><link href="js/yui/assets/skins/sam/autocomplete.css" rel="stylesheet" type="text/css"/><link href="js/yui/assets/skins/sam/calendar.css" rel="stylesheet" type="text/css"/><link href="skins/standard/global.css" rel="stylesheet" type="text/css"/><link href="skins/standard/show_bug.css" rel="stylesheet" type="text/css"/><!--[if lte IE 7]>
      


  <link href="skins/standard/IE-fixes.css" rel="stylesheet"
        type="text/css" >
<![endif]-->
<link href="skins/contrib/Gnome/global.css" rel="stylesheet" title="Gnome" type="text/css"/>
<script src="js/yui/yahoo-dom-event/yahoo-dom-event.js" type="text/javascript"></script><script src="js/yui/cookie/cookie-min.js" type="text/javascript"></script><script src="js/yui/datasource/datasource-min.js" type="text/javascript"></script><script src="js/yui/connection/connection-min.js" type="text/javascript"></script><script src="js/yui/json/json-min.js" type="text/javascript"></script><script src="js/yui/autocomplete/autocomplete-min.js" type="text/javascript"></script><script src="js/yui/calendar/calendar-min.js" type="text/javascript"></script><script src="js/global.js" type="text/javascript"></script>
<script type="text/javascript">
    <!--
        YAHOO.namespace('bugzilla');
        YAHOO.util.Event.addListener = function (el, sType, fn, obj, overrideContext) {
               if ( ("onpagehide" in window || YAHOO.env.ua.gecko) && sType === "unload") { sType = "pagehide"; };
               var capture = ((sType == "focusin" || sType == "focusout") && !YAHOO.env.ua.ie) ? true : false;
               return this._addListener(el, this._getType(sType), fn, obj, overrideContext, capture);
         };
        if ( "onpagehide" in window || YAHOO.env.ua.gecko) {
            YAHOO.util.Event._simpleRemove(window, "unload", 
                                           YAHOO.util.Event._unload);
        }
        
        function unhide_language_selector() { 
            YAHOO.util.Dom.removeClass(
                'lang_links_container', 'bz_default_hidden'
            ); 
        } 
        YAHOO.util.Event.onDOMReady(unhide_language_selector);

        
        var BUGZILLA = {
            param: {
                cookiepath: '\/',
                maxusermatches: 50
            },
            constant: {
                COMMENT_COLS: 80
            },
            string: {
                

                attach_desc_required:
                    'You must enter a Description for this attachment.',
                component_required:
                    'You must select a Component for this bug.',
                description_required:
                    'You must enter a Description for this bug.',
                short_desc_required:
                    'You must enter a Summary for this bug.',
                version_required:
                    'You must select a Version for this bug.'
            }
        };

    if (history && history.replaceState) {
      if(!document.location.href.match(/show_bug\.cgi/)) {
        history.replaceState( null, 
                             "Bug 629146 – Guide to writing babl extensions",  
                             "show_bug.cgi?id=629146" );
        document.title = "Bug 629146 – Guide to writing babl extensions";
      }
      if (document.location.href.match(/show_bug\.cgi\?.*list_id=/)) {
        var href = document.location.href;
        href = href.replace(/[\?&]+list_id=(\d+|cookie)/, '');
        history.replaceState(null, "Bug 629146 – Guide to writing babl extensions", href);
      }
    }
    YAHOO.util.Event.onDOMReady(function() {
      initDirtyFieldTracking();
    });
    // -->
    </script>
<script src="js/util.js" type="text/javascript"></script><script src="js/field.js" type="text/javascript"></script>
<link href="./search_plugin.cgi" rel="search" title="GNOME Bugzilla" type="application/opensearchdescription+xml"/>
<link href="images/favicon.ico" rel="shortcut icon"/><link href="extensions/TraceParser/web/style.css" rel="stylesheet" type="text/css"/>
</head>
<body class="bugzilla-gnome-org bz_bug bz_status_RESOLVED bz_product_GEGL bz_component_babl bz_bug_629146 yui-skin-sam" onload="">
<div id="header">
<div id="banner">
</div>
<table border="0" cellpadding="0" cellspacing="0" id="titles">
<tr>
<td id="title">
<p>GNOME Bugzilla – Bug 629146</p>
</td>
<td id="subtitle">
<p class="subheader">Guide to writing babl extensions</p>
</td>
<td id="information">
<p class="header_addl_info">Last modified: 2018-05-22 12:05:14 UTC</p>
</td>
</tr>
</table>
<table cellpadding="0" cellspacing="0" class="bz_default_hidden" id="lang_links_container"><tr><td>
</td></tr></table>
<ul class="links"><li><a href="./">Home</a></li></ul>
</div>
<div id="bugzilla-body">
<div style="margin-bottom: 50px; margin-top: 50px; padding: 40px; text-align: center; background-color: rgb(74,134,207); border: rgb(57,104,161); color: rgb(255,255,255); line-height: 300%;">After an <a href="https://wiki.gnome.org/Initiatives/DevelopmentInfrastructure" style="color: rgb(255,255,255);">evaluation</a>, GNOME has moved from Bugzilla to <a href="https://gitlab.gnome.org/" style="color: rgb(255,255,255);">GitLab</a>. <a href="https://wiki.gnome.org/GitLab" style="color: rgb(255,255,255);">Learn more about GitLab</a>. <br/><span style="background-color: #EE0000; font-size: xx-large;"><b>No new issues can be reported in GNOME Bugzilla anymore.</b></span>
<br/><span style="background-color: #EE0000; font-size: xx-large;"><b>To report an issue in a GNOME project, <a href="https://gitlab.gnome.org/GNOME" style="color: rgb(255,255,255);">go to GNOME GitLab</a></b>.</span><br/>Do <b>not</b> go to GNOME Gitlab for: <a href="https://sourceforge.net/p/bluefish/tickets" style="color: rgb(255,255,255);">Bluefish</a>, <a href="https://github.com/doxygen/doxygen/issues" style="color: rgb(255,255,255);">Doxygen</a>, <a href="https://bugs.gnucash.org/" style="color: rgb(255,255,255);">GnuCash</a>, <a href="https://gitlab.freedesktop.org/gstreamer/" style="color: rgb(255,255,255);">GStreamer</a>, <a href="https://github.com/afcowie/java-gnome/issues" style="color: rgb(255,255,255);">java-gnome</a>, <a href="https://github.com/ldtp/ldtp2/issues" style="color: rgb(255,255,255);">LDTP</a>, <a href="https://gitlab.freedesktop.org/NetworkManager/NetworkManager/issues" style="color: rgb(255,255,255);">NetworkManager</a>, <a href="https://github.com/tomboy-notes/tomboy/issues" style="color: rgb(255,255,255);">Tomboy</a>.</div>
<script type="text/javascript">
<!--

//-->
</script>
<form action="process_bug.cgi" id="changeform" method="post" name="changeform">
<input name="delta_ts" type="hidden" value="2018-05-22 12:05:14"/>
<input name="longdesclength" type="hidden" value="9"/>
<input name="id" type="hidden" value="629146"/>
<input name="token" type="hidden" value="1626106609-HeIO6jHTYgVQihhVJRUG5qG0BNKQwMo-cmUq1NLBYkw"/>
<div class="bz_alias_short_desc_container edit_form">
<a href="show_bug.cgi?id=629146"><b>Bug 629146</b></a> -<span class="bz_default_hidden" id="summary_alias_container">
<span id="short_desc_nonedit_display">Guide to writing babl extensions</span>
</span>
<div id="summary_alias_input">
<table id="summary">
<tr>
<td colspan="2">
</td>
</tr>
<tr><th class="field_label" id="field_label_short_desc">
<label accesskey="s" for="short_desc">
<a class="field_help_link" href="page.cgi?id=fields.html#short_desc" title="The bug summary is a short sentence which succinctly describes what the bug is about.">Summary:</a>
</label>
</th>
<td>Guide to writing babl extensions
          </td>
</tr>
</table>
</div>
</div>
<script type="text/javascript">
    hideAliasAndSummary('Guide to writing babl extensions', '');
  </script>
<table class="edit_form">
<tr>
<td class="bz_show_bug_column" id="bz_show_bug_column_1">
<table>
<tr>
<th class="field_label">
<a href="page.cgi?id=fields.html#bug_status">Status</a>:
    </th>
<td id="bz_field_status">
<span id="static_bug_status">RESOLVED
          OBSOLETE
      </span>
</td>
</tr>
<tr>
<td class="bz_section_spacer" colspan="2"></td>
</tr>
<tr><th class="field_label" id="field_label_product">
<a class="field_help_link" href="describecomponents.cgi" title="Bugs are categorised into Products and Components. Select a Classification to narrow down this list.">Product:</a>
</th>
<td class="field_value" id="field_container_product">GEGL</td>
</tr>
<tr class="bz_default_hidden"><th class="field_label" id="field_label_classification">
<a class="field_help_link" href="page.cgi?id=fields.html#classification" title="Bugs are categorised into Classifications, Products and Components. classifications is the top-level categorisation.">Classification:</a>
</th>
<td class="field_value" id="field_container_classification">Other</td>
</tr>
<tr><th class="field_label" id="field_label_component">
<a class="field_help_link" href="describecomponents.cgi?product=GEGL" title="Components are second-level categories; each belongs to a particular Product. Select a Product to narrow down this list.">Component:</a>
</th>
<td class="field_value" id="field_container_component">babl</td>
</tr>
<tr><th class="field_label" id="field_label_version">
<label for="version">
<a class="field_help_link" href="page.cgi?id=fields.html#version" title="The version field defines the version of the software the bug was found in.">Version:</a>
</label>
</th>
<td>git master
  </td>
</tr>
<tr><th class="field_label" id="field_label_rep_platform">
<label accesskey="h" for="rep_platform">
<a class="field_help_link" href="page.cgi?id=fields.html#rep_platform" title='The hardware platform the bug was observed on. Note: When searching, selecting the option "All" only finds bugs whose value for this field is literally the word "All".'>Hardware:</a>
</label>
</th>
<td class="field_value">Other
       All
      </td>
</tr>
<tr>
<td class="bz_section_spacer" colspan="2"></td>
</tr>
<tr>
<th class="field_label">
<label accesskey="i" for="priority">
<a href="page.cgi?id=fields.html#importance"><u>I</u>mportance</a></label>:
      </th>
<td>Normal
       enhancement
      </td>
</tr>
<tr>
<th class="field_label">
<label for="target_milestone">
<a href="page.cgi?id=fields.html#target_milestone">
            Target Milestone</a></label>:
        </th><td>---
  </td>
</tr>
<tr>
<th class="field_label">
<a href="page.cgi?id=fields.html#assigned_to">Assigned To</a>:
      </th>
<td><span class="vcard"><span class="fn">Default Gegl Component Owner</span>
</span>
</td>
</tr>
<tr><th class="field_label" id="field_label_qa_contact">
<label accesskey="q" for="qa_contact">
<a class="field_help_link" href="page.cgi?id=fields.html#qa_contact" title="The person responsible for confirming this bug if it is unconfirmed, and for verifying the fix once the bug has been resolved.">QA Contact:</a>
</label>
</th>
<td><span class="vcard"><span class="fn">Default Gegl Component Owner</span>
</span>
</td>
</tr>
<script type="text/javascript">
      assignToDefaultOnChange(['product', 'component'],
        'bugs\x40gegl.org',
        'bugs\x40gegl.org');
    </script>
<tr>
<td class="bz_section_spacer" colspan="2"></td>
</tr>
<tr><th class="field_label" id="field_label_bug_file_loc">
<label accesskey="u" for="bug_file_loc">
<a class="field_help_link" href="page.cgi?id=fields.html#bug_file_loc" title="Bugs can have a URL associated with them - for example, a pointer to a web site where the problem is seen.">URL:</a>
</label>
</th>
<td>
<span id="bz_url_input_area">
</span>
</td>
</tr>
<tr><th class="field_label" id="field_label_status_whiteboard">
<label accesskey="w" for="status_whiteboard">
<a class="field_help_link" href="page.cgi?id=fields.html#status_whiteboard" title="Each bug has a free-form single line text entry box for adding tags and status information.">Whiteboard:</a>
</label>
</th><td colspan="2">
</td>
</tr>

<tr>
<td class="bz_section_spacer" colspan="2"></td>
</tr>
<tr><th class="field_label" id="field_label_dependson">
<a class="field_help_link" href="page.cgi?id=fields.html#dependson" title="The bugs listed here must be resolved before this bug can be resolved.">Depends on:</a>
</th>
<td>
<span id="dependson_input_area">
</span>
</td>
</tr>
<tr><th class="field_label" id="field_label_blocked">
<a class="field_help_link" href="page.cgi?id=fields.html#blocked" title="This bug must be resolved before the bugs listed in this field can be resolved.">Blocks:</a>
</th>
<td>
<span id="blocked_input_area">
</span>
</td>
</tr>
<tr>
<th> </th>

</tr>
</table>
</td>
<td>
<div class="bz_column_spacer"> </div>
</td>
<td class="bz_show_bug_column" id="bz_show_bug_column_2">
<table cellpadding="3" cellspacing="1">
<tr>
<th class="field_label">
      Reported:
    </th>
<td>2010-09-09 09:17 UTC by <span class="vcard"><span class="fn">Rupert Weber</span>
</span>
</td>
</tr>
<tr>
<th class="field_label">
      Modified:
    </th>
<td>2018-05-22 12:05 UTC 
    </td>
</tr>

<tr>
<td class="bz_section_spacer" colspan="2"></td>
</tr>
<tr><th class="field_label" id="field_label_see_also">
<a class="field_help_link" href="page.cgi?id=fields.html#see_also" title="This allows you to refer to bugs in other installations. You can enter a URL to a bug in the 'Add Bug URLs' field to note that that bug is related to this one. You can enter multiple URLs at once by separating them with a comma. You should normally use this field to refer to bugs in other installations. For bugs in this installation, it is better to use the Depends on and Blocks fields.">See Also:</a>
</th>
<td class="field_value" id="field_container_see_also"></td>
</tr>
<tr><th class="field_label" id="field_label_cf_gnome_target">
<a class="field_help_link" href="page.cgi?id=fields.html#cf_gnome_target" title="A custom Drop Down field in this installation of GNOME Bugzilla.">GNOME target:</a>
</th>
<td class="field_value" colspan="2" id="field_container_cf_gnome_target">---</td>
</tr>
<tr><th class="field_label" id="field_label_cf_gnome_version">
<a class="field_help_link" href="page.cgi?id=fields.html#cf_gnome_version" title="A custom Drop Down field in this installation of GNOME Bugzilla.">GNOME version:</a>
</th>
<td class="field_value" colspan="2" id="field_container_cf_gnome_version">---</td>
</tr>
<tr>
<td class="bz_section_spacer" colspan="2"></td>
</tr>
</table>
</td>
</tr>
<tr>
<td colspan="3">
<hr id="bz_top_half_spacer"/>
</td>
</tr>
</table>
<table cellpadding="0" cellspacing="0" id="bz_big_form_parts"><tr>
<td>
<script type="text/javascript">
<!--
function toggle_display(link) {
    var table = document.getElementById("attachment_table");
    var view_all = document.getElementById("view_all");
    var hide_obsolete_url_parameter = "&hide_obsolete=1";
    // Store current height for scrolling later
    var originalHeight = table.offsetHeight;
    var rows = YAHOO.util.Dom.getElementsByClassName(
        'bz_tr_obsolete', 'tr', table);

    for (var i = 0; i < rows.length; i++) {
        bz_toggleClass(rows[i], 'bz_default_hidden');
    }

    if (YAHOO.util.Dom.hasClass(rows[0], 'bz_default_hidden')) {
        link.innerHTML = "Show Obsolete";
        view_all.href = view_all.href + hide_obsolete_url_parameter 
    }
    else {
        link.innerHTML = "Hide Obsolete";
        view_all.href = view_all.href.replace(hide_obsolete_url_parameter,"");
    }

    var newHeight = table.offsetHeight;
    // This scrolling makes the window appear to not move at all.
    window.scrollBy(0, newHeight - originalHeight);

    return false;
}
//-->
</script>
<br/>
<table cellpadding="4" cellspacing="0" id="attachment_table">
<tr id="a0">
<th align="left" colspan="3">
      Attachments
    </th>
</tr>
<tr class="bz_contenttype_text_plain bz_patch bz_tr_obsolete bz_default_hidden" id="a1">
<td valign="top">
<a href="attachment.cgi?id=169838" title="View the content of the attachment">
<b><span class="bz_obsolete">Extension guide</span></b></a>
<span class="bz_attach_extra_info">
              (39.62 KB,
                patch)

            <br/>
<a href="#attach_169838" title="Go to the comment associated with the attachment">2010-09-09 09:17 UTC</a>,

            <span class="vcard"><span class="fn">Rupert Weber</span>
</span>
</span>
</td>
<td class="bz_attach_status" valign="top">none
          </td>
<td valign="top">
<a href="attachment.cgi?id=169838&amp;action=edit">Details</a>  |
    <a href="review?bug=629146&amp;attachment=169838">Review</a>
</td>
</tr>
<tr class="bz_contenttype_text_plain bz_patch bz_tr_obsolete bz_default_hidden" id="a2">
<td valign="top">
<a href="attachment.cgi?id=170599" title="View the content of the attachment">
<b><span class="bz_obsolete">Updated draft for extension guide / howto</span></b></a>
<span class="bz_attach_extra_info">
              (48.54 KB,
                patch)

            <br/>
<a href="#attach_170599" title="Go to the comment associated with the attachment">2010-09-19 16:10 UTC</a>,

            <span class="vcard"><span class="fn">Rupert Weber</span>
</span>
</span>
</td>
<td class="bz_attach_status" valign="top">none
          </td>
<td valign="top">
<a href="attachment.cgi?id=170599&amp;action=edit">Details</a>  |
    <a href="review?bug=629146&amp;attachment=170599">Review</a>
</td>
</tr>
<tr class="bz_contenttype_text_plain bz_patch" id="a3">
<td valign="top">
<a href="attachment.cgi?id=171488" title="View the content of the attachment">
<b>Another update</b></a>
<span class="bz_attach_extra_info">
              (49.05 KB,
                patch)

            <br/>
<a href="#attach_171488" title="Go to the comment associated with the attachment">2010-10-01 12:57 UTC</a>,

            <span class="vcard"><span class="fn">Rupert Weber</span>
</span>
</span>
</td>
<td class="bz_attach_status" valign="top">needs-work
          </td>
<td valign="top">
<a href="attachment.cgi?id=171488&amp;action=edit">Details</a>  |
    <a href="review?bug=629146&amp;attachment=171488">Review</a>
</td>
</tr>

</table>
<br/>
</td>
<td>
</td>
</tr></table>
<div id="comments"><script src="js/comments.js" type="text/javascript">
</script>
<script type="text/javascript">
<!--
  /* Adds the reply text to the `comment' textarea */
  function replyToComment(id, real_id, name) {
      var prefix = "(In reply to " + name + " from comment #" + id + ")\n";
      var replytext = "";
        /* pre id="comment_name_N" */
        var text_elem = document.getElementById('comment_text_'+id);
        var text = getText(text_elem);
        replytext = prefix + wrapReplyText(text);


      /* <textarea id="comment"> */
      var textarea = document.getElementById('comment');
      if (textarea.value != replytext) {
          textarea.value += replytext;
      }

      textarea.focus();
  } 
//-->
</script>
<!-- This auto-sizes the comments and positions the collapse/expand links 
     to the right. -->
<table cellpadding="0" cellspacing="0" class="bz_comment_table"><tr>
<td>
<div class="bz_comment bz_first_comment" id="c0">
<div class="bz_first_comment_head">
<span class="bz_comment_number">
<a href="show_bug.cgi?id=629146#c0">Description</a>
</span>
<span class="bz_comment_user">
<span class="vcard"><span class="fn">Rupert Weber</span>
</span>
</span>
<span class="bz_comment_user_images">
</span>
<span class="bz_comment_time">
          2010-09-09 09:17:09 UTC
        </span>
</div>
<pre class="bz_comment_text">Created <span class="bz_obsolete"><a href="attachment.cgi?id=169838" name="attach_169838" title="Extension guide">attachment 169838</a> <a href="attachment.cgi?id=169838&amp;action=edit" title="Extension guide">[details]</a></span> <a href="review?bug=629146&amp;attachment=169838">[review]</a>
Extension guide

First draft of a guide to writing babl extensions, still quite a few questions open.

(This first patch also includes the two small patches I posted to #628561, because I was too lazy to convince git to apply without them)</pre>
</div><div class="bz_comment" id="c1">
<div class="bz_comment_head">
<span class="bz_comment_number">
<a href="show_bug.cgi?id=629146#c1">Comment 1</a>
</span>
<span class="bz_comment_user">
<span class="vcard"><span class="fn">Øyvind Kolås (pippin)</span>
</span>
</span>
<span class="bz_comment_user_images">
</span>
<span class="bz_comment_time">
          2010-09-09 14:50:17 UTC
        </span>
</div>
<pre class="bz_comment_text">Comments for the editnotes, it would have been easier to reply to these, in context if the questions were asked on the mailinglist rather than embedded in a patch.

linear, plane, planar

"linear" is for converting a buffer of n pixels with a given pixel format, the pixels are expected to contain the components packed/interleaved/chunky in the order specified in the pixel format.

"plane" is for converting a single plane, this can be used internally by babl when constructing reference/fallback conversions, it allows doing processing on one and one component and as guessed with individual pitch changes (to skip the other components if needed).

"planar" is for converting full images where the components might be stored separately, like it is done for some YCbCr formats in video compression (where the dimensions of the images for the Cb and Cr components might be smaller than the Y image), this feature in babl is not used by GEGL and migh not be completely functional.

...
yes, you should re-register color models and used pixel formats in extensions that are not present in the babl-base, since the order extensions are loaded in are not guaranteed.

...
the use of babl_type_new and ... "integer", "unsigned" indeed seems to be wrong.

..
the return value of conversion functions was intended to be the number of samples actually converted, the value is not really used anywhere in babl though and could probably be removed and be made void.

..

the "chroma", "alpha" and "luma" flags/component meta data have no use inside babl but can be useful in determining behavior for image processing algorithms in a manner that is independent of the actual color model used. At one point it was also used to detmine whether conversion would incurr a loss of "features" thus blacklisting possible multi-step conversion paths, this is now achieved through other means.</pre>
</div><div class="bz_comment" id="c2">
<div class="bz_comment_head">
<span class="bz_comment_number">
<a href="show_bug.cgi?id=629146#c2">Comment 2</a>
</span>
<span class="bz_comment_user">
<span class="vcard"><span class="fn">Rupert Weber</span>
</span>
</span>
<span class="bz_comment_user_images">
</span>
<span class="bz_comment_time">
          2010-09-19 16:10:31 UTC
        </span>
</div>
<pre class="bz_comment_text">Created <span class="bz_obsolete"><a href="attachment.cgi?id=170599" name="attach_170599" title="Updated draft for extension guide / howto">attachment 170599</a> <a href="attachment.cgi?id=170599&amp;action=edit" title="Updated draft for extension guide / howto">[details]</a></span> <a href="review?bug=629146&amp;attachment=170599">[review]</a>
Updated draft for extension guide / howto

Updated the guide.
Still needs some work, but I think it's slowly getting there.

(still includes the two small patches  to <a class="bz_bug_link bz_status_RESOLVED bz_closed" href="show_bug.cgi?id=628561" title="RESOLVED FIXED - Small corrections to babl docs">bug 628561</a>)

For a quick peek without patching, it's also posted to
<a href="http://leguanease.org/gimp/babl/docs/extension-guide-main.html">http://leguanease.org/gimp/babl/docs/extension-guide-main.html</a></pre>
</div><div class="bz_comment" id="c3">
<div class="bz_comment_head">
<span class="bz_comment_number">
<a href="show_bug.cgi?id=629146#c3">Comment 3</a>
</span>
<span class="bz_comment_user">
<span class="vcard"><span class="fn">Rupert Weber</span>
</span>
</span>
<span class="bz_comment_user_images">
</span>
<span class="bz_comment_time">
          2010-10-01 12:57:12 UTC
        </span>
</div>
<pre class="bz_comment_text">Created <span class=""><a href="attachment.cgi?id=171488" name="attach_171488" title="Another update">attachment 171488</a> <a href="attachment.cgi?id=171488&amp;action=edit" title="Another update">[details]</a></span> <a href="review?bug=629146&amp;attachment=171488">[review]</a>
Another update

Update to the guide, main addition is the reference vs. shortcut conversion.</pre>
</div><div class="bz_comment" id="c4">
<div class="bz_comment_head">
<span class="bz_comment_number">
<a href="show_bug.cgi?id=629146#c4">Comment 4</a>
</span>
<span class="bz_comment_user">
<span class="vcard"><span class="fn">Jon Nordby</span>
</span>
</span>
<span class="bz_comment_user_images">
</span>
<span class="bz_comment_time">
          2011-03-28 22:56:05 UTC
        </span>
</div>
<pre class="bz_comment_text">Could you update the patch now patches to <a class="bz_bug_link bz_status_RESOLVED bz_closed" href="show_bug.cgi?id=628561" title="RESOLVED FIXED - Small corrections to babl docs">bug 628561</a> has been committed, and split out the CSS change in a separate patch?</pre>
</div><div class="bz_comment" id="c5">
<div class="bz_comment_head">
<span class="bz_comment_number">
<a href="show_bug.cgi?id=629146#c5">Comment 5</a>
</span>
<span class="bz_comment_user">
<span class="vcard"><span class="fn">Tobias Mueller</span>
</span>
</span>
<span class="bz_comment_user_images">
</span>
<span class="bz_comment_time">
          2012-01-19 20:21:11 UTC
        </span>
</div>
<pre class="bz_comment_text">Rupert, ping</pre>
</div><div class="bz_comment" id="c6">
<div class="bz_comment_head">
<span class="bz_comment_number">
<a href="show_bug.cgi?id=629146#c6">Comment 6</a>
</span>
<span class="bz_comment_user">
<span class="vcard"><span class="fn">André Klapper</span>
</span>
</span>
<span class="bz_comment_user_images">
</span>
<span class="bz_comment_time">
          2012-02-14 09:25:42 UTC
        </span>
</div>
<pre class="bz_comment_text">[Setting QA Contact to 'bugs at gegl.org' so people interested in following GEGL bug reports' changes can still follow if the assignee changes to a real person.]</pre>
</div><div class="bz_comment" id="c7">
<div class="bz_comment_head">
<span class="bz_comment_number">
<a href="show_bug.cgi?id=629146#c7">Comment 7</a>
</span>
<span class="bz_comment_user">
<span class="vcard"><span class="fn">Tobias Endrigkeit</span>
</span>
</span>
<span class="bz_comment_user_images">
</span>
<span class="bz_comment_time">
          2012-10-14 18:47:33 UTC
        </span>
</div>
<pre class="bz_comment_text">Only Update for the Patch is needed, there is no missing information of the bug</pre>
</div><div class="bz_comment" id="c8">
<div class="bz_comment_head">
<span class="bz_comment_number">
<a href="show_bug.cgi?id=629146#c8">Comment 8</a>
</span>
<span class="bz_comment_user">
<span class="vcard"><span class="fn">GNOME Infrastructure Team</span>
</span>
</span>
<span class="bz_comment_user_images">
</span>
<span class="bz_comment_time">
          2018-05-22 12:05:14 UTC
        </span>
</div>
<pre class="bz_comment_text">-- GitLab Migration Automatic Message --

This bug has been migrated to GNOME's GitLab instance and has been closed from further activity.

You can subscribe and participate further through the new bug through this link to our GitLab instance: <a href="https://gitlab.gnome.org/GNOME/gegl/issues/10">https://gitlab.gnome.org/GNOME/gegl/issues/10</a>.</pre>
</div>
<script type="text/javascript">
  function traceparser_toggle_trace(link, trace_id) {
    var trace = document.getElementById('trace_' + trace_id);
    bz_toggleClass(trace, 'bz_default_hidden');
    if (link.innerHTML == '+') { link.innerHTML = '&mdash;'; }
    else { link.innerHTML = '+'; }
  }
</script>
</td>
<td>
</td>
</tr></table>
</div>
<hr/>
</form>
<hr/>

<br/>
</div>
<div id="footer">
<div class="intro"></div>
<ul id="useful-links">
<li id="links-actions"><ul class="links">
<li><a href="./">Home</a></li>
<li><span class="separator">| </span><span style="text-decoration:line-through"><a href="">New</a></span></li>
<li><span class="separator">| </span><a href="page.cgi?id=browse.html&amp;product=GEGL">Browse</a></li>
<li><span class="separator">| </span><a href="query.cgi">Search</a></li>
<li class="form">
<span class="separator">| </span>
<form action="buglist.cgi" method="get" onsubmit="if (this.quicksearch.value == '')
                  { alert('Please enter one or more search terms first.');
                    return false; } return true;">
<input id="no_redirect_bottom" name="no_redirect" type="hidden" value="0"/>
<script type="text/javascript">
      if (history && history.replaceState) {
        var no_redirect = document.getElementById("no_redirect_bottom");
        no_redirect.value = 1;
      }
    </script>
<input class="txt" id="quicksearch_bottom" name="quicksearch" title="Quick Search" type="text" value=""/>
<input class="btn" id="find_bottom" type="submit" value="Search"/></form>
<a href="page.cgi?id=quicksearch.html" title="Quicksearch Help">[?]</a></li>
<li><span class="separator">| </span><a href="page.cgi?id=reports.html">Reports</a></li>
<li>
<span class="separator">| </span>
<a href="http://www.bugzilla.org/docs/4.4/en/html/bug_page.html" target="_blank">Help</a>
</li>
<li id="mini_login_container_bottom">
<span class="separator">| </span>
<a href="show_bug.cgi?id=629146&amp;GoAheadAndLogIn=1" id="login_link_bottom" onclick="return show_mini_login_form('_bottom')">Log In</a>
<form action="show_bug.cgi?id=629146" class="mini_login bz_default_hidden" id="mini_login_bottom" method="POST" onsubmit="return check_mini_login_fields( '_bottom' );">
<input class="bz_login" id="Bugzilla_login_bottom" name="Bugzilla_login" onfocus="mini_login_on_focus('_bottom')" title="Login"/>
<input class="bz_password" id="Bugzilla_password_bottom" name="Bugzilla_password" title="Password" type="password"/>
<input class="bz_password bz_default_hidden bz_mini_login_help" id="Bugzilla_password_dummy_bottom" onfocus="mini_login_on_focus('_bottom')" title="Password" type="text" value="password"/>
<input checked="" class="bz_remember" id="Bugzilla_remember_bottom" name="Bugzilla_remember" type="checkbox" value="on"/>
<label for="Bugzilla_remember_bottom">Remember</label>
<input name="Bugzilla_login_token" type="hidden" value=""/>
<input id="log_in_bottom" name="GoAheadAndLogIn" type="submit" value="Log in"/>
<script type="text/javascript">
      mini_login_constants = {
          "login" : "login",
          "warning" : "You must set the login and password before logging in."
      };
      
      if (YAHOO.env.ua.gecko || YAHOO.env.ua.ie || YAHOO.env.ua.opera) {
          YAHOO.util.Event.onDOMReady(function() {
              init_mini_login_form('_bottom');
          });
      }
      else {
          YAHOO.util.Event.on(window, 'load', function () {
              window.setTimeout(function() {
                  init_mini_login_form('_bottom');
              }, 200);
          });
    }
    </script>
<a href="#" onclick="return hide_mini_login_form('_bottom')">[x]</a>
</form>
</li>
<li id="forgot_container_bottom">
<span class="separator">| </span>
<a href="show_bug.cgi?id=629146&amp;GoAheadAndLogIn=1#forgot" id="forgot_link_bottom" onclick="return show_forgot_form('_bottom')">Forgot Password</a>
<form action="token.cgi" class="mini_forgot bz_default_hidden" id="forgot_form_bottom" method="post">
<label for="login_bottom">Login:</label>
<input id="login_bottom" name="loginname" size="20" type="text"/>
<input id="forgot_button_bottom" type="submit" value="Reset Password"/>
<input name="a" type="hidden" value="reqpw"/>
<input id="token_bottom" name="token" type="hidden" value="1626106609-yE4gN6goBqk5gAUrcStMAhzx2e_2pEN1JmuGm3eYWCE"/>
<a href="#" onclick="return hide_forgot_form('_bottom')">[x]</a>
</form>
</li>
</ul>
</li>
</ul>
<div class="outro"></div>
</div>
</body>
</html>